extends Node

func _on_Button_pressed():
	var scene_path = "res://StressScene.tscn"
	
	#var resource = load(scene_path)
	var loader = ResourceLoader.load_interactive(scene_path)
	
	var state = loader.poll()
	while state != ERR_FILE_EOF:
		state = loader.poll()
		print(state)
	var resource = loader.get_resource()
	var scene = resource.instance()
	
	$Control.queue_free()
	add_child(scene)
